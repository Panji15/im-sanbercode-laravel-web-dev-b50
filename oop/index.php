<?php
  require_once('animal.php');
  require_once('frog.php');
  require_once('ape.php');

    $animal = new Animal("Shaun");
    echo "Name : " .$animal->name;  // "Shaun"
    echo "<br>Legs : " .$animal->legs;  // 4
    echo "<br>Coold Bloded : " .$animal->cold_blooded;  // "no"

    echo "<br><br>";

    $frog = new Frog("Buduk");
    echo "Name : " .$frog->name;  // "Buduk"
    echo "<br>Legs : " .$frog->legs;  // 4
    echo "<br>Coold Bloded : " .$frog->cold_blooded;  // "no"
    $frog->jump(); // "hop hop"

    echo "<br><br>";

    $ape = new Ape("Kera Sakti");
    echo "Name : " .$ape->name;  // "Kera Sakti"
    echo "<br>Legs : " .$ape->legs;  // 2
    echo "<br>Coold Bloded : " .$ape->cold_blooded;  // "no"
    $ape->yell(); // "auooo"


?>
